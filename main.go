package main

import (
	"database/sql"
	"encoding/gob"
	"log"
	"net/http"
	"os"

	"bitbucket.org/ajiyakin/aji_yakin_test/handler"
	"bitbucket.org/ajiyakin/aji_yakin_test/helper"
	"bitbucket.org/ajiyakin/aji_yakin_test/storage"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/sessions"
)

func main() {
	// init global config
	store := sessions.NewCookieStore([]byte("alsk6jf8oe3urq5eio"))
	sessionAge := 60 // session will be expired in a minute
	var mailFrom string

	// init mail configuration
	mailFrom = os.Getenv("MY_MAIL_FROM")
	if mailFrom == "" {
		log.Fatal("env. var MY_MAIL_FROM and MY_MAIL_PASS is required")
		os.Exit(1)
	}

	// init database configuration
	dbUser := os.Getenv("MY_DB_USER")
	dbPass := os.Getenv("MY_DB_PASS")
	dbName := os.Getenv("MY_DB_NAME")
	if dbUser == "" || dbPass == "" || dbName == "" {
		log.Fatal("env. var MY_DB_USER, MY_DB_PASS and MY_DB_NAME is required")
		os.Exit(1)
	}

	// init global session configuration
	store.Options = &sessions.Options{
		Path:   "/",
		MaxAge: sessionAge,
	}

	// init session package user
	helper.Store = store
	helper.SessionAge = sessionAge

	// init config at handler
	handler.MailFrom = mailFrom
	handler.Store = store

	// init config at middleware (handler package)
	handler.Store = store
	handler.SessionAge = sessionAge

	// register User struct so the session would be able to store it
	gob.Register(&storage.User{})

	// init config at storage
	storage.DbUser = dbUser
	storage.DbPass = dbPass
	storage.DbName = dbName
	// init database connection
	var err error
	storage.DbConn, err = sql.Open("mysql", dbUser+":"+dbPass+"@/"+dbName)
	if err != nil {
		log.Fatal("error connect to database")
	}

	// init and register handlers
	handlers := handler.InitHandlers()
	http.Handle("/", handlers)

	log.Fatal(http.ListenAndServe(":8080", http.DefaultServeMux))
}
