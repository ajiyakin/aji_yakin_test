FROM golang

ADD . /go/src/bitbucket.org/ajiyakin/aji_yakin_test

RUN go get github.com/go-sql-driver/mysql && \
    go get github.com/gorilla/sessions && \
    go get golang.org/x/crypto/bcrypt

RUN cd /go/src/bitbucket.org/ajiyakin/aji_yakin_test && \
    go clean && \
    go build

CMD cd /go/src/bitbucket.org/ajiyakin/aji_yakin_test && \
    ./aji_yakin_test

EXPOSE 8080
