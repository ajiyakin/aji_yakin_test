package handler

import (
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/ajiyakin/aji_yakin_test/response"
	"bitbucket.org/ajiyakin/aji_yakin_test/storage"
)

type middleware func(http.HandlerFunc) http.HandlerFunc

// MiddlewareLogger prints the request to the console
func MiddlewareLogger(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		h(w, r)
		execDur := time.Since(start)
		fmt.Printf("%s %s %v\n", r.Method, r.URL.RequestURI(), execDur)
	}
}

// AuthMiddleware is an authentication middleware to determine whether user is
// already signed in or not, if user not yet sign in, it will automatically send
// an unauthorized response otherwise, continue to next handler
func AuthMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// check whether user is logged in or not
		session, err := Store.Get(r, "auth-cookie")
		if err != nil {
			response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
			return
		}
		// reset session age so the session will only expired if no activity
		session.Options.MaxAge = SessionAge
		if err = session.Save(r, w); err != nil {
			fmt.Printf("session error at auth middleware: %v", err)
			response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
			return
		}
		userRaw := session.Values["authenticated"]
		if userRaw == nil {
			response.WriteJSONError(w, http.StatusUnauthorized, "User unauthorized")
			return
		}
		_, ok := userRaw.(*storage.User)
		if !ok {
			response.WriteJSONError(w, http.StatusUnauthorized, "User unauthorized")
			return
		}
		next.ServeHTTP(w, r)
	})
}
