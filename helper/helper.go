package helper

import (
	"encoding/json"
	"errors"
	"net/http"
	"regexp"
	"strconv"
	"time"

	"github.com/gorilla/sessions"

	"bitbucket.org/ajiyakin/aji_yakin_test/storage"
)

var (
	// Store is cookie storage
	Store *sessions.CookieStore
	// SessionAge max of session life
	SessionAge int
)

// VerifyGoogleToken get verifictation from google for specified token id
func VerifyGoogleToken(idToken string) (storage.User, error) {
	// get info from google token server
	gURL := "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + idToken
	res, err := http.Get(gURL)
	if err != nil {
		return storage.User{}, err
	}
	decoder := json.NewDecoder(res.Body)
	var gRes struct {
		ISS           string `json:"iss"`
		Sub           string `json:"sub"`
		AZP           string `json:"azp"`
		AUD           string `json:"aup"`
		IAT           string `json:"iat"`
		Exp           string `json:"exp"`
		Email         string `json:"email"`
		EmailVerified string `json:"email_verified"`
		Name          string `json:"name"`
	}
	// TODO Verify iss, sub, etc
	err = decoder.Decode(&gRes)
	if err != nil {
		return storage.User{}, err
	}
	// check if token is expired or not
	t := time.Now()
	expInt, err := strconv.ParseInt(gRes.Exp, 10, 64)
	if err != nil {
		return storage.User{}, err
	}
	exp := time.Unix(expInt, 0)
	if exp.Unix() < t.Unix() {
		return storage.User{}, errors.New("session expired")
	}
	return storage.User{
		Email:    gRes.Email,
		FullName: gRes.Name,
		GoogleID: gRes.Sub,
	}, nil
}

// StoreToCookie stores user data into cookie, only for storing
// user data to authentication cookie
func StoreToCookie(u storage.User, w http.ResponseWriter, r *http.Request) error {
	session, err := Store.Get(r, "auth-cookie")
	// session expired within 1 minute
	session.Options.MaxAge = SessionAge
	if err != nil {
		return err
	}
	// "authenticated" key is container for user info itself
	session.Values["authenticated"] = &u
	return session.Save(r, w)
}

// ClearCookie clear current user logged in from cookie
func ClearCookie(w http.ResponseWriter, r *http.Request) error {
	session, err := Store.Get(r, "auth-cookie")
	if err != nil {
		return err
	}
	session.Values["authenticated"] = nil
	session.Options.MaxAge = -1
	return session.Save(r, w)
}

// ValidateEmail check whether email format is valid or not
func ValidateEmail(email string) error {
	emailRegexp := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	if !emailRegexp.MatchString(email) {
		return errors.New("Invalid email address")
	}
	return nil
}
