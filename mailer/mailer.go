package mailer

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"os/user"
	"path/filepath"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	gmail "google.golang.org/api/gmail/v1"
)

// tokenFromFile retrieves a Token from a given file path.
// It returns the retrieved Token and any read error encountered.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	t := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(t)
	defer f.Close()
	return t, err
}

// tokenCacheFile generates credential file path/filename.
// It returns the generated credential path/filename.
func tokenCacheFile() (string, error) {
	usr, err := user.Current()
	if err != nil {
		return "", err
	}
	tokenCacheDir := filepath.Join(usr.HomeDir, ".credentials")
	err = os.MkdirAll(tokenCacheDir, 0700)
	if err != nil {
		return "", err
	}
	return filepath.Join(tokenCacheDir,
		url.QueryEscape("gmail-go-quickstart.json")), err
}

// getClient uses a Context and Config to retrieve a Token
// then generate a Client. It returns the generated Client.
func getClient(ctx context.Context, config *oauth2.Config) (*http.Client, error) {
	cacheFile, err := tokenCacheFile()
	if err != nil {
		fmt.Printf("Unable to get path to cached credential file. %v\n", err)
		return nil, err
	}
	tok, err := tokenFromFile(cacheFile)
	if err != nil {
		fmt.Printf("failed to get token from file: %v\n", err)
		fmt.Println("please use token generator cli under subfolder ./generator to generate starter token")
		return nil, err
	}
	return config.Client(ctx, tok), nil
}

// SendMail sends email via Gmail API with OAuth2
func SendMail(from, to, subject, msgBody string) error {
	ctx := context.Background()

	usr, err := user.Current()
	if err != nil {
		return err
	}
	secretCreds := filepath.Join(usr.HomeDir, "client_secret.json")

	b, err := ioutil.ReadFile(secretCreds)
	if err != nil {
		fmt.Printf("Unable to read client secret file: %v\n", err)
		return err
	}

	// If modifying these scopes, delete your previously saved credentials
	// at ~/.credentials/gmail-go-quickstart.json
	config, err := google.ConfigFromJSON(b, gmail.GmailSendScope)
	if err != nil {
		fmt.Printf("Unable to parse client secret file to config: %v\n", err)
		return err
	}
	client, err := getClient(ctx, config)
	if err != nil {
		return err
	}

	srv, err := gmail.New(client)
	if err != nil {
		fmt.Printf("Unable to retrieve gmail Client %v\n", err)
		return err
	}

	var message gmail.Message
	messageStr := []byte("From: " + from + "\r\n" +
		"To: " + to + "\r\n" +
		"Subject: " + subject + "\r\n\r\n" +
		msgBody,
	)
	message.Raw = base64.URLEncoding.EncodeToString(messageStr)
	msgSent, err := srv.Users.Messages.Send("me", &message).Do()
	if err != nil {
		fmt.Printf("Failed to send email to user: %v\n", err)
		return err
	}
	fmt.Printf("Email sent: %v\n", msgSent)
	return nil
}
