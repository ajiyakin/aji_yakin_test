package response

import (
	"encoding/json"
	"net/http"
)

// ErrorMsg model for displaying error message
type ErrorMsg struct {
	Code    int
	Message string
}

// WriteJSON writes JSON data to the response writer
// including JSON content type header
func WriteJSON(w http.ResponseWriter, statusCode int, data interface{}) {
	enc := json.NewEncoder(w)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	if err := enc.Encode(data); err != nil {
		http.Error(w, "Internal Server Error", 500)
		return
	}
}

// WriteJSONError writes error errMsg within JSON format
func WriteJSONError(w http.ResponseWriter, code int, msg string) {
	WriteJSON(w, code, ErrorMsg{
		Code:    code,
		Message: msg,
	})
}
