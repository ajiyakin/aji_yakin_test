package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/ajiyakin/aji_yakin_test/helper"
	"bitbucket.org/ajiyakin/aji_yakin_test/mailer"
	"bitbucket.org/ajiyakin/aji_yakin_test/response"
	"bitbucket.org/ajiyakin/aji_yakin_test/storage"

	"golang.org/x/crypto/bcrypt"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
)

var (
	// MailFrom setting email sender used for reset password functionality
	MailFrom string
	// Store cookie storer
	Store *sessions.CookieStore
	// SessionAge max life of session
	SessionAge int
)

type handler struct {
	name        string
	path        string
	method      string
	handlerFunc http.HandlerFunc
	middlewares []middleware
}

// InitHandlers instansitate handler (router)
func InitHandlers() *mux.Router {
	routers := []handler{
		{
			name:        "api_login",
			path:        "/api/login",
			method:      http.MethodPost,
			handlerFunc: LoginHandler,
			middlewares: []middleware{MiddlewareLogger},
		},
		{
			name:        "api_register",
			path:        "/api/register",
			method:      http.MethodPost,
			handlerFunc: RegisterHandler,
			middlewares: []middleware{MiddlewareLogger},
		},
		{
			name:        "api_logout",
			path:        "/api/logout",
			method:      http.MethodGet,
			handlerFunc: LogoutHandler,
			middlewares: []middleware{AuthMiddleware, MiddlewareLogger},
		},
		{
			name:        "api_renewpass_request",
			path:        "/api/reset/request",
			method:      http.MethodPost,
			handlerFunc: RequestResetHandler,
			middlewares: []middleware{MiddlewareLogger},
		},
		{
			name:        "api_renewpass_action",
			path:        "/api/reset",
			method:      http.MethodPost,
			handlerFunc: ResetPasswordHandler,
			middlewares: []middleware{MiddlewareLogger},
		},
		{
			name:        "api_profile_get",
			path:        "/api/profile",
			method:      http.MethodGet,
			handlerFunc: GetProfileHandler,
			middlewares: []middleware{AuthMiddleware, MiddlewareLogger},
		},
		{
			name:        "api_profile_update",
			path:        "/api/profile",
			method:      http.MethodPut,
			handlerFunc: UpdateProfileHandler,
			middlewares: []middleware{AuthMiddleware, MiddlewareLogger},
		},
	}

	router := mux.NewRouter().StrictSlash(true)

	for _, h := range routers {
		finalHandler := h.handlerFunc
		for _, m := range h.middlewares {
			finalHandler = m(finalHandler)
		}
		router.Methods(h.method).
			Name(h.name).
			Path(h.path).
			Handler(finalHandler)
	}

	// serve static resources -- frontend
	staticDir := "static/"
	router.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir(staticDir))))

	return router
}

// LoginHandler handle user login via POST request
func LoginHandler(w http.ResponseWriter, r *http.Request) {
	// handle login with google
	if r.Header.Get("Content-Type") == "application/x-www-form-urlencoded" {
		idToken := r.FormValue("id_token")
		gUser, err := helper.VerifyGoogleToken(idToken)
		if err != nil {
			fmt.Println(err)
			response.WriteJSONError(w, http.StatusUnauthorized, "Failed to get authorization from Google")
			return
		}
		u, err := storage.GetUserByEmail(gUser.Email)
		u.GoogleID = gUser.GoogleID
		if err != nil {
			fmt.Println(err)
			response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
			return
		}
		// if user not found, insert
		if u.ID < 1 {
			u = gUser
			u.ID, err = storage.InsertUser(u)
			if err != nil {
				fmt.Println(err)
				response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
				return
			}
		}
		err = helper.StoreToCookie(u, w, r)
		if err != nil {
			fmt.Println(err)
			response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
			return
		}
		u.Password = "" // don't send password
		response.WriteJSON(w, http.StatusOK, u)
		return
	}
	// login manualy via username and password form
	decoder := json.NewDecoder(r.Body)
	var u storage.User
	err := decoder.Decode(&u)
	if err != nil {
		response.WriteJSONError(w, http.StatusBadRequest, "Body request should be in JSON format")
		return
	}
	if u.Username == "" || u.Password == "" {
		response.WriteJSONError(w, http.StatusUnauthorized, "Username and password should not be empty")
		return
	}
	// get user by username and password from database
	u, err = storage.GetUserByUsernameOrEmailAndPass(u.Username, u.Username, u.Password)
	if err != nil {
		fmt.Println(err)
		response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}
	// user with those username and pass not found
	if u.ID < 1 {
		response.WriteJSONError(w, http.StatusUnauthorized, "Username and password combination not found")
		return
	}
	err = helper.StoreToCookie(u, w, r)
	if err != nil {
		response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}
	u.Password = "" // don't send raw password
	response.WriteJSON(w, http.StatusOK, u)
	return
}

// RegisterHandler handle user registration
func RegisterHandler(w http.ResponseWriter, r *http.Request) {
	if r.Body == nil {
		response.WriteJSONError(w, http.StatusBadRequest, "Request body should not be nil")
		return
	}
	decoder := json.NewDecoder(r.Body)
	var newUser struct {
		storage.User
		VerifyPassword string `json:"verify_password"`
	}
	err := decoder.Decode(&newUser)
	if err != nil {
		response.WriteJSONError(w, http.StatusBadRequest, "Body request should be in JSON format")
		return
	}
	if newUser.Password == "" || newUser.Email == "" {
		response.WriteJSONError(w, http.StatusUnprocessableEntity, "Password and email should not be empty")
		return
	}
	if err := helper.ValidateEmail(newUser.Email); err != nil {
		response.WriteJSONError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	if newUser.Password != newUser.VerifyPassword {
		response.WriteJSONError(w, http.StatusUnprocessableEntity, "Password and verify password doesn't match")
		return
	}
	// save to database
	u := newUser.User
	u.ID, err = storage.InsertUser(u)
	if err != nil {
		response.WriteJSONError(w, http.StatusUnprocessableEntity, fmt.Sprintf("error save to database: %v", err))
		return
	}
	u.Password = "" // don't send raw password
	response.WriteJSON(w, http.StatusCreated, u)
}

// LogoutHandler handle user logout action by deleting the cookie data
func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	err := helper.ClearCookie(w, r)
	if err != nil {
		response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}
	response.WriteJSON(w, http.StatusOK, nil)
}

// RequestResetHandler handle for request to reset password:
// generate token and send it user's email
func RequestResetHandler(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var rUser storage.User
	err := decoder.Decode(&rUser)
	if err != nil {
		response.WriteJSONError(w, http.StatusBadRequest, "Body request should be in JSON format")
		return
	}
	if rUser.Email == "" {
		response.WriteJSONError(w, http.StatusBadRequest, "Email is required")
		return
	}
	if err := helper.ValidateEmail(rUser.Email); err != nil {
		response.WriteJSONError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	u, err := storage.GetUserByEmail(rUser.Email)
	if err != nil {
		response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}
	// user not found
	if u.ID < 1 {
		response.WriteJSONError(w, http.StatusNotFound, "User with email "+rUser.Email+" not found")
		return
	}
	// generate token
	token, err := bcrypt.GenerateFromPassword([]byte(u.Email+u.Password), bcrypt.DefaultCost)
	if err != nil {
		response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
		fmt.Printf("error generate reset password token: %v\n", err)
		return
	}
	// send token link for reset password to user via email
	tokenLink := fmt.Sprintf("%s/reset_password.html?token=%s&email=%s\n", r.Host, token, u.Email)
	msgBody := "To reset password please go to (copy and paste the link): " + tokenLink
	err = mailer.SendMail(MailFrom, u.Email, "Reset Password LoginLogoutApp", msgBody)
	if err != nil {
		response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
		fmt.Println(err)
		return
	}
	response.WriteJSON(w, http.StatusOK, nil)
}

// ResetPasswordHandler handle for reset password verified via token.
// token itself is a hash built from email+current_password
func ResetPasswordHandler(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var b struct {
		Email             string `json:"email"`
		Token             string `json:"token"`
		NewPassword       string `json:"new_password"`
		VerifyNewPassword string `json:"verify_new_password"`
	}
	err := decoder.Decode(&b)
	if err != nil {
		response.WriteJSONError(w, http.StatusBadRequest, "Body request should be in JSON format")
		return
	}
	if b.Email == "" || b.Token == "" || b.NewPassword == "" || b.VerifyNewPassword == "" {
		response.WriteJSONError(w, http.StatusBadRequest, "Email, token, new password and verify new password should not be empty")
		return
	}
	if b.VerifyNewPassword != b.NewPassword {
		response.WriteJSONError(w, http.StatusUnprocessableEntity, "New password and Verify new password doesn't match")
		return
	}
	var u storage.User
	u, err = storage.GetUserByEmail(b.Email)
	if err != nil {
		response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}
	// user not found
	if u.ID < 1 {
		response.WriteJSONError(w, http.StatusNotFound, "User not found")
		return
	}
	// compare token, if doesn't match, don't update
	if err := bcrypt.CompareHashAndPassword([]byte(b.Token), []byte(u.Email+u.Password)); err != nil {
		response.WriteJSONError(w, http.StatusUnprocessableEntity, "Invalid token")
		return
	}
	// update user's password if token match
	err = storage.UpdatePassword(b.NewPassword, u.ID)
	if err != nil {
		response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}
	u.Password = "" // don't send raw password
	response.WriteJSON(w, http.StatusOK, u)
}

// GetProfileHandler handle GET request for current loged in user's profile data
func GetProfileHandler(w http.ResponseWriter, r *http.Request) {
	session, err := Store.Get(r, "auth-cookie")
	if err != nil {
		response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}
	userRaw := session.Values["authenticated"]
	user, ok := userRaw.(*storage.User)
	if !ok {
		response.WriteJSONError(w, http.StatusUnauthorized, "User unauthorized")
		return
	}
	id := user.ID
	// get user from database by it's ID
	var u storage.User
	u, err = storage.GetUserByID(id)
	if err != nil {
		fmt.Printf("error get user: %v\n", err)
		response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}
	// user not found
	if u.ID < 1 {
		response.WriteJSONError(w, http.StatusNotFound, "User not found")
		return
	}
	u.Password = "" // don't send raw password
	response.WriteJSON(w, http.StatusOK, u)
}

// UpdateProfileHandler handle PUT request for current loged in user's profile data
// to be able to update the profile data
func UpdateProfileHandler(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var u storage.User
	err := decoder.Decode(&u)
	fmt.Println(err)
	if err != nil {
		response.WriteJSONError(w, http.StatusBadRequest, "Body request should be in JSON format")
		return
	}
	// update user by it's id
	err = storage.UpdateUser(u)
	if err != nil {
		response.WriteJSONError(w, http.StatusUnprocessableEntity, fmt.Sprintf("Error when saving to database: %v", err))
		return
	}
	u, err = storage.GetUserByID(u.ID)
	if err != nil {
		response.WriteJSONError(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}
	if u.ID < 1 {
		response.WriteJSONError(w, http.StatusNotFound, "User not found")
		return
	}
	u.Password = "" // don't send raw password
	response.WriteJSON(w, http.StatusOK, u)
}
