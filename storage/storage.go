package storage

import "database/sql"

var (
	// DbConn database connection
	DbConn *sql.DB
	// DbUser user of dabase
	DbUser string
	// DbPass database password
	DbPass string
	// DbName database name used by this apps
	DbName string
)

// User model
type User struct {
	ID        int64  `json:"id"`
	Username  string `json:"username"`
	Password  string `json:"password"`
	GoogleID  string `json:"google_id"`
	FullName  string `json:"full_name"`
	Address   string `json:"address"`
	Email     string `json:"email"`
	Telephone string `json:"telephone"`
}

// InsertUser insert new user to database
func InsertUser(u User) (int64, error) {
	result, err := DbConn.Exec(
		"INSERT INTO `users` (`username`,  `password`, `email`, `full_name`, `address`, `telephone`, `google_id`) VALUES (?, ?, ?, ?, ?, ?, ?)",
		u.Username, u.Password, u.Email,
		u.FullName, u.Address, u.Telephone, u.GoogleID)
	if err != nil {
		return 0, err
	}
	lastID, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return lastID, nil
}

// UpdateUser update user data by it's id without updating password data
// to update password, use updatePassword instead
func UpdateUser(u User) error {
	_, err := DbConn.Exec(
		"UPDATE `users` SET `username`=?, `email`=?, `full_name`=?, `address`=?, `telephone`=?, `google_id`=? WHERE `id`=?",
		u.Username, u.Email, u.FullName, u.Address, u.Telephone, u.GoogleID, u.ID)
	return err
}

// UpdatePassword update user's password by user's id
func UpdatePassword(newPass string, id int64) error {
	_, err := DbConn.Exec("UPDATE `users` SET `password`=? WHERE `id`=?", newPass, id)
	return err
}

// GetUserByID return selectd user data by it's ID
func GetUserByID(id int64) (User, error) {
	rows, err := DbConn.Query("SELECT u.id, u.username, u.password, u.email, u.full_name, u.address, u.telephone, u.google_id FROM users u WHERE u.id=?", id)
	if err != nil {
		return User{}, err
	}
	var u User
	if rows.Next() {
		err = rows.Scan(&u.ID, &u.Username, &u.Password, &u.Email, &u.FullName, &u.Address, &u.Telephone, &u.GoogleID)
		if err != nil {
			return User{}, err
		}
		return u, nil
	}
	return User{}, nil
}

// GetUserByUsernameAndPass get user data by it's username and pass
func GetUserByUsernameAndPass(username, password string) (User, error) {
	rows, err := DbConn.Query("SELECT u.id, u.username, u.password, u.email, u.full_name, u.address, u.telephone, u.google_id FROM users u WHERE u.username=? AND u.password=?", username, password)
	if err != nil {
		return User{}, err
	}
	var u User
	if rows.Next() {
		err = rows.Scan(&u.ID, &u.Username, &u.Password, &u.Email, &u.FullName, &u.Address, &u.Telephone, &u.GoogleID)
		if err != nil {
			return User{}, err
		}
		return u, nil
	}
	return User{}, nil
}

// GetUserByEmail return selected user data by it's email
func GetUserByEmail(email string) (User, error) {
	rows, err := DbConn.Query("SELECT u.id, u.username, u.password, u.email, u.full_name, u.address, u.telephone, u.google_id FROM users u WHERE u.email=?", email)
	if err != nil {
		return User{}, err
	}
	var u User
	if rows.Next() {
		err = rows.Scan(&u.ID, &u.Username, &u.Password, &u.Email, &u.FullName, &u.Address, &u.Telephone, &u.GoogleID)
		if err != nil {
			return User{}, err
		}
		return u, nil
	}
	return User{}, nil
}

// GetUserByUsernameOrEmailAndPass select user based on username or email
// that matches with the password
func GetUserByUsernameOrEmailAndPass(username, email, password string) (User, error) {
	rows, err := DbConn.Query("SELECT u.id, u.username, u.password, u.email, u.full_name, u.address, u.telephone, u.google_id FROM users u WHERE (u.username=? AND u.password=?) OR (u.email=? AND u.password=?)",
		username, password, email, password)
	if err != nil {
		return User{}, err
	}
	var u User
	if rows.Next() {
		err = rows.Scan(&u.ID, &u.Username, &u.Password, &u.Email, &u.FullName, &u.Address, &u.Telephone, &u.GoogleID)
		if err != nil {
			return User{}, err
		}
		return u, nil
	}
	return User{}, nil
}
