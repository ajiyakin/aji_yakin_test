# Login Logout App #

Simple LoginLogout with profile updater


## Libs and techs ##

### Backend ###

Golang with libraries:

- [gorilla/mux](http://www.gorillatoolkit.org/pkg/mux)
- [gorilla/sessions](http://www.gorillatoolkit.org/pkg/sessions)
- [Google's Gmail API](google.golang.org/api/gmail/v1)
- [OAuth2](golang.org/x/oauth2/)

### Frontend ###

- [Bootstrap 3: CSS Framework](http://getbootstrap.com/)
- [jQuery: JavaScript Libraries](https://jquery.com/)
